**Maranatha Medical Plaza Valdosta**

At Maranatha Medical Plaza we are experts in the management of asthma, COPD, bronchiectasis, sarcoidosis, lung cancers, and other less common problems like interstitial lung diseases and pulmonary hypertension. Our adult medicine service strives to provide the best primary care and health maintenance available for patients with hypertension, diabetes, arthritis, hyperlipidemia, and many other common health concerns. Maranatha Medical Plaza Valdosta has been committed to providing the best quality health care to improve their patients’ overall wellness and quality of life.
[https://www.maranathamd.com/](https://www.maranathamd.com/)
